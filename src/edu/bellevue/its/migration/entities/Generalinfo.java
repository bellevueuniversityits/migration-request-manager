package edu.bellevue.its.migration.entities;
// Generated Jan 31, 2013 8:41:31 AM by Hibernate Tools 3.2.1.GA


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Generalinfo generated by hbm2java
 */
@Entity
@Table(name="generalinfo"
    ,catalog="migrationrequest"
)
public class Generalinfo  implements java.io.Serializable {


     private Integer idGeneralInfo;
     private String pillar;
     private String ticketNumber;
     private String description;
     private String purpose;
     private String requestor;
     private Date requestedDate;
     private Integer businessAnalyst;
     private String tester;
     private String securityRequired;
     private String techReviewReq;
     private String modifiedFiles;
     private String projectName;
     private String notifySd;
     private String notifyOs;
     private Set<Request> requests = new HashSet<Request>(0);

    public Generalinfo() {
    }

    public Generalinfo(String pillar, String ticketNumber, String description, String purpose, String requestor, Date requestedDate, Integer businessAnalyst, String tester, String securityRequired, String techReviewReq, String modifiedFiles, String projectName, String notifySd, String notifyOs, Set<Request> requests) {
       this.pillar = pillar;
       this.ticketNumber = ticketNumber;
       this.description = description;
       this.purpose = purpose;
       this.requestor = requestor;
       this.requestedDate = requestedDate;
       this.businessAnalyst = businessAnalyst;
       this.tester = tester;
       this.securityRequired = securityRequired;
       this.techReviewReq = techReviewReq;
       this.modifiedFiles = modifiedFiles;
       this.projectName = projectName;
       this.notifySd = notifySd;
       this.notifyOs = notifyOs;
       this.requests = requests;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)
    
    @Column(name="idGeneralInfo", unique=true, nullable=false)
    public Integer getIdGeneralInfo() {
        return this.idGeneralInfo;
    }
    
    public void setIdGeneralInfo(Integer idGeneralInfo) {
        this.idGeneralInfo = idGeneralInfo;
    }
    
    @Column(name="pillar", length=45)
    public String getPillar() {
        return this.pillar;
    }
    
    public void setPillar(String pillar) {
        this.pillar = pillar;
    }
    
    @Column(name="ticketNumber", length=45)
    public String getTicketNumber() {
        return this.ticketNumber;
    }
    
    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }
    
    @Column(name="description")
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    @Column(name="purpose")
    public String getPurpose() {
        return this.purpose;
    }
    
    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }
    
    @Column(name="requestor")
    public String getRequestor() {
        return this.requestor;
    }
    
    public void setRequestor(String requestor) {
        this.requestor = requestor;
    }
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="requestedDate", length=19)
    public Date getRequestedDate() {
        return this.requestedDate;
    }
    
    public void setRequestedDate(Date requestedDate) {
        this.requestedDate = requestedDate;
    }
    
    @Column(name="businessAnalyst")
    public Integer getBusinessAnalyst() {
        return this.businessAnalyst;
    }
    
    public void setBusinessAnalyst(Integer businessAnalyst) {
        this.businessAnalyst = businessAnalyst;
    }
    
    @Column(name="tester")
    public String getTester() {
        return this.tester;
    }
    
    public void setTester(String tester) {
        this.tester = tester;
    }
    
    @Column(name="securityRequired", length=1)
    public String getSecurityRequired() {
        return this.securityRequired;
    }
    
    public void setSecurityRequired(String securityRequired) {
        this.securityRequired = securityRequired;
    }
    
    @Column(name="techReviewReq", length=1)
    public String getTechReviewReq() {
        return this.techReviewReq;
    }
    
    public void setTechReviewReq(String techReviewReq) {
        this.techReviewReq = techReviewReq;
    }
    
    @Column(name="modifiedFiles")
    public String getModifiedFiles() {
        return this.modifiedFiles;
    }
    
    public void setModifiedFiles(String modifiedFiles) {
        this.modifiedFiles = modifiedFiles;
    }
    
    @Column(name="projectName")
    public String getProjectName() {
        return this.projectName;
    }
    
    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
    
    @Column(name="notifySD", length=1)
    public String getNotifySd() {
        return this.notifySd;
    }
    
    public void setNotifySd(String notifySd) {
        this.notifySd = notifySd;
    }
    
    @Column(name="notifyOS", length=1)
    public String getNotifyOs() {
        return this.notifyOs;
    }
    
    public void setNotifyOs(String notifyOs) {
        this.notifyOs = notifyOs;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="generalinfo")
    public Set<Request> getRequests() {
        return this.requests;
    }
    
    public void setRequests(Set<Request> requests) {
        this.requests = requests;
    }




}


