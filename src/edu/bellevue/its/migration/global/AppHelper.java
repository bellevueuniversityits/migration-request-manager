/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bellevue.its.migration.global;

import edu.bellevue.its.migration.entities.Role;
import edu.bellevue.its.migration.entities.User;
import java.awt.Point;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import javax.swing.JInternalFrame;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author tj
 */
public class AppHelper {
    
    
    public static byte[] encodeHTML(String s)
    {
        try{
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            GZIPOutputStream out = new GZIPOutputStream(baos);
            out.write(s.getBytes());
            out.flush();
            out.close();
            baos.close();

            byte[] compData = baos.toByteArray();

            return compData;
        }catch(Exception e){}
        
        return null;
    }
    
    public static String decodeHTML(byte[] b)
    {
        try
        {
            ByteArrayInputStream bais = new ByteArrayInputStream(b);
            GZIPInputStream in = new GZIPInputStream(bais);
            
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            for (int value = 0; value != -1;) {
                value = in.read();
                if (value != -1) {
                    baos.write(value);
                }
            }
            in.close();
            baos.close();
            
            return new String(baos.toByteArray(),"UTF-8");
        }catch(Exception e){}
        return "";
    }
    
    public static String encodePassword(String userName, String pass)
    {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] digest = md.digest((userName + pass + "migrationManager").getBytes());
            
            BigInteger bi = new BigInteger(digest);

            // Format to hexadecimal
            String s = bi.toString(16); 
            if (s.length() % 2 != 0) {
                s = "0" + s;
            }  
            
            return s;
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }
    
    public static Role getRoleByName(String role)
    {
        Session s = AppGlobals.getInstance().hibernateSession;
        
        Role aRole = (Role) s.createCriteria(Role.class).add(Restrictions.eq("roleName",role)).uniqueResult();
        return aRole;
    }
    public static User getUserById(Integer userId) {
        Session session = AppGlobals.getInstance().hibernateSession;
        User aUser = (User) session.createCriteria(User.class).add(Restrictions.eq("idUser",userId)).uniqueResult();
        return aUser;
    }

    public static boolean userHasRole( String rolename)
    {
        Role r = AppHelper.getRoleByName(rolename);
        return (AppGlobals.getInstance().loggedInUser.getRoles().contains(r));
    }
    public static User getUserByUserNameAndPassword(String username, String password)
    {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] digest = md.digest((username + password + "migrationManager").getBytes());
            
            BigInteger bi = new BigInteger(digest);

            // Format to hexadecimal
            String encodedPass = bi.toString(16); 
            if (encodedPass.length() % 2 != 0) {
                encodedPass = "0" + encodedPass;
            }  
            
            Session session = AppGlobals.getInstance().hibernateSession;
            User aUser = (User) session.createCriteria(User.class).add(Restrictions.eq("userName",username)).add(Restrictions.eq("password", encodedPass)).uniqueResult();
            
            return aUser;
            
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static String getUserDisplayName(User loggedInUser) {
        return loggedInUser.getFirstName() + " " + loggedInUser.getLastName();
    }

    public static void placeRightOfParent(JInternalFrame child, JInternalFrame owner) {
        Point parentPoint = owner.getLocation();
        Point newPoint = (Point)parentPoint.clone();
        newPoint.x += owner.getSize().width + 5;
        
        int heightDiff = owner.getSize().height - child.getSize().height;
        
        if (newPoint.y + (heightDiff / 2) >=0)
        {
            newPoint.y += heightDiff / 2;
        }
        child.setLocation(newPoint);
        
    }
    public boolean userHasRole(User u, Role r)
    {
        Iterator<Role> roles = u.getRoles().iterator();
        while (roles.hasNext())
        {
            Role nextRole = roles.next();
            if (nextRole.equals(r))
            {
                return true;
            }
        }
        return false;
    }
    
    public static User getUserByUsername(String username)
    {
        Session session = AppGlobals.getInstance().hibernateSession;
        User aUser = (User) session.createCriteria(User.class).add(Restrictions.eq("userName",username)).uniqueResult();
        return aUser;
    }

}
