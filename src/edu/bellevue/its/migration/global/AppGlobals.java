/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bellevue.its.migration.global;

import edu.bellevue.its.migration.gui.dialogs.LoginDialog;
import edu.bellevue.its.migration.entities.User;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;

/**
 *
 * @author tslater
 */
public class AppGlobals {
    
    public User loggedInUser;
    public Session hibernateSession;
    
    private static AppGlobals instance;
    
    public static AppGlobals getInstance()
    {
        if (instance == null) {
            instance = new AppGlobals();
            instance.hibernateSession = HibernateUtil.getSessionFactory().openSession();
        }
        
        return instance;
    }
    
    public boolean login()
    {
        
        LoginDialog dialog = new LoginDialog(new javax.swing.JFrame(), true);
        dialog.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent e) {
                System.exit(0);
            }
        });
        dialog.setVisible(true);
        
        while (dialog.isVisible())
        {
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(AppGlobals.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return true;
    }
}
