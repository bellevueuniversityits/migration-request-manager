/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bellevue.its.migration.gui;

import edu.bellevue.its.migration.entities.Generalinfo;
import edu.bellevue.its.migration.entities.User;
import edu.bellevue.its.migration.global.AppGlobals;
import edu.bellevue.its.migration.gui.frames.developer.EditRequestFrame;
import edu.bellevue.its.migration.gui.frames.admin.RoleManagementFrame;
import edu.bellevue.its.migration.gui.frames.admin.UserManagementFrame;
import edu.bellevue.its.migration.global.AppHelper;
import edu.bellevue.its.migration.gui.frames.migrator.MigratorOverviewFrame;
import edu.bellevue.its.migration.gui.frames.security.SecurityOverviewFrame;
import java.awt.Frame;
import javax.swing.JDesktopPane;
import javax.swing.UIManager;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author tslater
 */
public class MainApp extends javax.swing.JFrame {

    /**
     * Creates new form MainApp
     */
    public MainApp() {
        
        try {
            UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
            String lookFeel = UIManager.getLookAndFeel().getClass().getName();
            System.out.println(lookFeel);
        } 
        catch (Exception e){
           // handle exception
        }
        AppGlobals.getInstance().login();
        
        initComponents();

        this.setTitle("Migration Management: ( " + AppHelper.getUserDisplayName(AppGlobals.getInstance().loggedInUser) +  " )");
        System.out.println("Post Login");
        openDefaultFrames();
    }

    public void openDefaultFrames()
    {
        User u = AppGlobals.getInstance().loggedInUser;
        
        if (u.getRoles().contains(AppHelper.getRoleByName("Admin")))
        {
            UserManagementFrame umf = new UserManagementFrame();
            desktop.add(umf);
            umf.setVisible(true);
            
            RoleManagementFrame rmf = new RoleManagementFrame();
            desktop.add(rmf);
            rmf.setVisible(true);
        }
    }
    
    public static JDesktopPane getDesktop()
    {
        return desktop;
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator1 = new javax.swing.JSeparator();
        desktop = new javax.swing.JDesktopPane();
        jMenuBar1 = new javax.swing.JMenuBar();
        mnuFile = new javax.swing.JMenu();
        mnuFileExit = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        mnuMigrator = new javax.swing.JMenu();
        mnuMigratorOpenRequests = new javax.swing.JMenuItem();
        mnuDeveloper = new edu.bellevue.its.migration.gui.controls.RoleSecuredMenu();
        mnuDeveloperNewRequest = new edu.bellevue.its.migration.gui.controls.RoleSecuredMenuItem();
        mnuSecurity = new javax.swing.JMenu();
        mnuSecurityViewRequests = new javax.swing.JMenuItem();
        mnuSecurityCompletedRequests = new javax.swing.JMenuItem();
        mnuAdmin = new edu.bellevue.its.migration.gui.controls.RoleSecuredMenu();
        mnuAdminUsers = new edu.bellevue.its.migration.gui.controls.RoleSecuredMenuItem();
        mnuAdminRoles = new edu.bellevue.its.migration.gui.controls.RoleSecuredMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setExtendedState(Frame.MAXIMIZED_BOTH);

        desktop.addContainerListener(new java.awt.event.ContainerAdapter() {
            public void componentAdded(java.awt.event.ContainerEvent evt) {
                desktopComponentAdded(evt);
            }
        });

        mnuFile.setText("File");

        mnuFileExit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_X, java.awt.event.InputEvent.ALT_MASK));
        mnuFileExit.setText("Exit");
        mnuFileExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuFileExitActionPerformed(evt);
            }
        });
        mnuFile.add(mnuFileExit);

        jMenuBar1.add(mnuFile);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        mnuMigrator.setText("Migrator");

        mnuMigratorOpenRequests.setText("View Open Requests");
        mnuMigratorOpenRequests.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuMigratorOpenRequestsActionPerformed(evt);
            }
        });
        mnuMigrator.add(mnuMigratorOpenRequests);

        jMenuBar1.add(mnuMigrator);

        mnuDeveloper.setText("Developer");

        mnuDeveloperNewRequest.setText("New Request...");
        mnuDeveloperNewRequest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuDeveloperNewRequestActionPerformed(evt);
            }
        });
        mnuDeveloper.add(mnuDeveloperNewRequest);

        jMenuBar1.add(mnuDeveloper);

        mnuSecurity.setText("Security");

        mnuSecurityViewRequests.setText("Outstanding Requests");
        mnuSecurityViewRequests.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuSecurityViewRequestsActionPerformed(evt);
            }
        });
        mnuSecurity.add(mnuSecurityViewRequests);

        mnuSecurityCompletedRequests.setText("Completed Requests");
        mnuSecurityCompletedRequests.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuSecurityCompletedRequestsActionPerformed(evt);
            }
        });
        mnuSecurity.add(mnuSecurityCompletedRequests);

        jMenuBar1.add(mnuSecurity);

        mnuAdmin.setText("Admin");
        mnuAdmin.setRoleName("Admin");

        mnuAdminUsers.setText("Manage Users");
        mnuAdminUsers.setRoleName("Admin");
        mnuAdminUsers.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuAdminUsersActionPerformed(evt);
            }
        });
        mnuAdmin.add(mnuAdminUsers);

        mnuAdminRoles.setText("Manage Roles");
        mnuAdminRoles.setRoleName("Admin");
        mnuAdminRoles.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuAdminRolesActionPerformed(evt);
            }
        });
        mnuAdmin.add(mnuAdminRoles);

        jMenuBar1.add(mnuAdmin);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(desktop, javax.swing.GroupLayout.DEFAULT_SIZE, 384, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(desktop, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 263, Short.MAX_VALUE)
        );

        setBounds(0, 0, 400, 323);
    }// </editor-fold>//GEN-END:initComponents

    private void mnuFileExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuFileExitActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_mnuFileExitActionPerformed

    private void mnuAdminUsersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuAdminUsersActionPerformed
        // TODO add your handling code here:
        UserManagementFrame umf = new UserManagementFrame();
        desktop.add(umf);
        umf.setVisible(true);
    }//GEN-LAST:event_mnuAdminUsersActionPerformed

    private void mnuAdminRolesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuAdminRolesActionPerformed
        // TODO add your handling code here:
        RoleManagementFrame rmf = new RoleManagementFrame();
        desktop.add(rmf);
        rmf.setVisible(true);
    }//GEN-LAST:event_mnuAdminRolesActionPerformed

    private void desktopComponentAdded(java.awt.event.ContainerEvent evt) {//GEN-FIRST:event_desktopComponentAdded
        // TODO add your handling code here:
        
        int compCount = desktop.getComponentCount();
        //evt.getChild().setLocation(30 * compCount, 30 * compCount);
    }//GEN-LAST:event_desktopComponentAdded

    private void mnuDeveloperNewRequestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuDeveloperNewRequestActionPerformed
        // TODO add your handling code here:
        EditRequestFrame erf = new EditRequestFrame();
        desktop.add(erf);
        erf.setVisible(true);
    }//GEN-LAST:event_mnuDeveloperNewRequestActionPerformed

    private void mnuSecurityViewRequestsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuSecurityViewRequestsActionPerformed
        // TODO add your handling code here:
        if (AppHelper.userHasRole("Security") || AppHelper.userHasRole("Admin"))
        {
            SecurityOverviewFrame sof = new SecurityOverviewFrame();
            MainApp.getDesktop().add(sof);
            sof.setVisible(true);
        }
    }//GEN-LAST:event_mnuSecurityViewRequestsActionPerformed

    private void mnuSecurityCompletedRequestsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuSecurityCompletedRequestsActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_mnuSecurityCompletedRequestsActionPerformed

    private void mnuMigratorOpenRequestsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuMigratorOpenRequestsActionPerformed
        // TODO add your handling code here:
        if (AppHelper.userHasRole("Migrator") || AppHelper.userHasRole("Admin"))
        {
            MigratorOverviewFrame mof = new MigratorOverviewFrame();
            MainApp.getDesktop().add(mof);
            mof.setVisible(true);
        }
    }//GEN-LAST:event_mnuMigratorOpenRequestsActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainApp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainApp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainApp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainApp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainApp().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private static javax.swing.JDesktopPane desktop;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JSeparator jSeparator1;
    private edu.bellevue.its.migration.gui.controls.RoleSecuredMenu mnuAdmin;
    private edu.bellevue.its.migration.gui.controls.RoleSecuredMenuItem mnuAdminRoles;
    private edu.bellevue.its.migration.gui.controls.RoleSecuredMenuItem mnuAdminUsers;
    private edu.bellevue.its.migration.gui.controls.RoleSecuredMenu mnuDeveloper;
    private edu.bellevue.its.migration.gui.controls.RoleSecuredMenuItem mnuDeveloperNewRequest;
    private javax.swing.JMenu mnuFile;
    private javax.swing.JMenuItem mnuFileExit;
    private javax.swing.JMenu mnuMigrator;
    private javax.swing.JMenuItem mnuMigratorOpenRequests;
    private javax.swing.JMenu mnuSecurity;
    private javax.swing.JMenuItem mnuSecurityCompletedRequests;
    private javax.swing.JMenuItem mnuSecurityViewRequests;
    // End of variables declaration//GEN-END:variables
}
