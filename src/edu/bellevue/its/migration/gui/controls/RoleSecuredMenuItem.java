/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bellevue.its.migration.gui.controls;

import edu.bellevue.its.migration.entities.Role;
import edu.bellevue.its.migration.entities.User;
import edu.bellevue.its.migration.global.AppGlobals;
import edu.bellevue.its.migration.global.AppHelper;
import javax.swing.JMenuItem;

/**
 *
 * @author tslater
 */
public class RoleSecuredMenuItem extends JMenuItem {
    private String roleName;
    
    public String getRoleName()
    {
        return roleName;
    }
    
    public void setRoleName(String role)
    {
        roleName = role;
        
        if (java.beans.Beans.isDesignTime()) {
            setEnabled(false);
        }
        else {
            setVisible(false);
        }
        
        if (AppGlobals.getInstance().loggedInUser != null)
        {
            Role r = AppHelper.getRoleByName(role);
            
            User curUser = AppGlobals.getInstance().loggedInUser;
            boolean enabled = curUser.getRoles().contains(r);
            
            if (java.beans.Beans.isDesignTime()) {
                setEnabled(enabled);
            }
            else {
                setVisible(enabled);
            }
            
            
        }
        
    }
}
