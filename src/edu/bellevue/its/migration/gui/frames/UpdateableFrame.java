/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bellevue.its.migration.gui.frames;

/**
 *
 * @author tslater
 */
public interface UpdateableFrame {
    public void updateView();
}
