/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.bellevue.its.migration.gui.frames.security;

import edu.bellevue.its.migration.entities.Completion;
import edu.bellevue.its.migration.entities.Generalinfo;
import edu.bellevue.its.migration.entities.Request;
import edu.bellevue.its.migration.global.AppGlobals;
import edu.bellevue.its.migration.global.AppHelper;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.swing.table.AbstractTableModel;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author tslater
 */
public class SecurityRequestTableModel extends AbstractTableModel{
    private String[] columnNames = { "Request #", "Author", "Pillar","Security Approved", "Last Environment", "Security Completed",""};
    private ArrayList<Request> requests = new ArrayList<Request>();
    
    public SecurityRequestTableModel()
    {
        super();
        if (java.beans.Beans.isDesignTime() == false)
        {
            Session session = AppGlobals.getInstance().hibernateSession;
            List generalinfoList = session.createCriteria(Generalinfo.class).add(Restrictions.eq("securityRequired","Y")).list();

            for (Object o : generalinfoList)
            {
                Generalinfo currGenInfo = (Generalinfo)o;
                Request r = currGenInfo.getRequests().toArray(new Request[0])[0];
                requests.add(r);
            }
        }
    }
    
    @Override
    public boolean isCellEditable(int row, int column) {
        return column == 6;
    }
    @Override
    public int getRowCount() {
        if (java.beans.Beans.isDesignTime())
        {
            return 1;
        }
        return requests.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex)
        {
            case 0:
                return requests.get(rowIndex).getIdRequest();
            case 1:
                return AppHelper.getUserDisplayName(requests.get(rowIndex).getUser());
            case 2:
                return requests.get(rowIndex).getGeneralinfo().getPillar();
            case 3:
                boolean approved = requests.get(rowIndex).getSecurityoverview().getSecurityApprover() > 0;
                return approved ? "Yes" : "No";
            case 4:
                if (requests.get(rowIndex).getCompletions().isEmpty())
                {
                    return "N/A";
                }else
                {
                    Set<Completion> completions = requests.get(rowIndex).getCompletions();
                    Iterator<Completion> iter = completions.iterator();
                    Date newestDate = null;
                    Completion latestCompletion = null;
                    while (iter.hasNext())
                    {
                        Completion curComp = iter.next();
                        Date curDate = iter.next().getDateCompleted();
                        if (newestDate == null || curDate.after(newestDate))
                        {
                            newestDate = curDate;
                            latestCompletion = curComp;
                        }
                    }
                    
                    if (latestCompletion != null)
                    {
                        return latestCompletion.getToSource();
                    } else
                    {
                        return "N/A";
                    }
                }
            case 5:
                if (requests.get(rowIndex).getCompletions().isEmpty())
                {
                    return "N/A";
                }else
                {
                    Set<Completion> completions = requests.get(rowIndex).getCompletions();
                    Iterator<Completion> iter = completions.iterator();
                    Date newestDate = null;
                    Completion latestCompletion = null;
                    while (iter.hasNext())
                    {
                        Completion curComp = iter.next();
                        Date curDate = iter.next().getDateCompleted();
                        if (newestDate == null || curDate.after(newestDate))
                        {
                            newestDate = curDate;
                            latestCompletion = curComp;
                        }
                    }
                    
                    if (latestCompletion != null)
                    {
                        return latestCompletion.getSecurityCompleted().equals("Y") ? "Yes" : "No";
                    } else
                    {
                        return "N/A";
                    }
                }
            case 6:
                return "View Request";
        }
        return "Test";
    }
    
    @Override
    public String getColumnName(int colNum)
    {
        return columnNames[colNum];
    }
    
}
